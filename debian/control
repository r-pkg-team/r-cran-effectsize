Source: r-cran-effectsize
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-effectsize
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-effectsize.git
Homepage: https://cran.r-project.org/package=effectsize
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-bayestestr (>= 0.15.0),
               r-cran-insight (>= 1.0.0),
               r-cran-parameters (>= 0.24.0),
               r-cran-performance (>= 0.12.4),
               r-cran-datawizard (>= 0.13.0)
Testsuite: autopkgtest-pkg-r

Package: r-cran-effectsize
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R indices of effect size and standardized parameters
 Provide utilities to work with indices of effect size and standardized
 parameters for a wide variety of models (see support list of insight;
 Lüdecke, Waggoner & Makowski (2019) <doi:10.21105/joss.01412>), allowing
 computation and conversion of indices such as Cohen's d, r, odds, etc.
